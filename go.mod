module gitlab.com/magtil/rpgio

go 1.20

require (
	github.com/HouzuoGuo/tiedot v0.0.0-20210905174726-ae1e16866d06
	github.com/denisenkom/go-mssqldb v0.12.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/g3n/engine v0.2.0
	github.com/go-sql-driver/mysql v1.7.1
	github.com/gookit/color v1.5.3
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/lib/pq v1.10.9
	go.etcd.io/bbolt v1.3.7
	modernc.org/ql v1.4.5
)

require (
	github.com/edsrzf/mmap-go v1.1.0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/image v0.0.0-20210607152325-775e3b0c77b9 // indirect
	golang.org/x/sys v0.6.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	modernc.org/b v1.0.4 // indirect
	modernc.org/db v1.0.6 // indirect
	modernc.org/file v1.0.5 // indirect
	modernc.org/fileutil v1.1.2 // indirect
	modernc.org/golex v1.0.5 // indirect
	modernc.org/internal v1.0.6 // indirect
	modernc.org/lldb v1.0.4 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/sortutil v1.1.0 // indirect
	modernc.org/strutil v1.1.3 // indirect
	modernc.org/zappy v1.0.5 // indirect
)
