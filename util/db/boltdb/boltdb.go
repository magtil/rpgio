package boltdb

import (
	"gitlab.com/magtil/rpgio/util/logger"
	bolt "go.etcd.io/bbolt"
)

type Conn struct {
	Db     *bolt.DB
	DbFile string
}

var (
	Logger     = logger.Logger{}
	DbFolder   = "./db/"
	DateFormat = "2006/01/02/15:04:05"
)

func (c *Conn) Open() {
	tdb, err := bolt.Open(c.DbFile, 0600, nil)
	if err != nil {
		Logger.Debug(err)
	} else {
		c.Db = tdb
		defer c.Db.Close()
	}
}

func (c *Conn) Close() {
	if err := c.Db.Close(); err != nil {
		Logger.Debug(err)
	}
}
