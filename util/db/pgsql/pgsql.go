package pgsql

import (
	"database/sql"
	"reflect"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/magtil/rpgio/util/logger"
)

type Conn struct {
	db        *sql.DB
	DbHost    string
	DbPort    string
	DbName    string
	DbUser    string
	DbPass    string
	DbCharset string
	DbEncrypt string
	DbPrefix  string
}

var (
	Logger     = logger.Logger{}
	DateFormat = "2006/01/02/15:04:05"
)

//打开数据库
func (c *Conn) Open() {
	//sslmode有效值
	//disable - No SSL
	//require - Always SSL (skip verification)
	//verify-ca - Always SSL (verify that the certificate presented by the server was signed by a trusted CA)
	//verify-full - Always SSL (verify that the certification presented by the server was signed by a trusted CA and the server host name matches the one in the certificate)
	dsn := "postgres://" + c.DbUser + ":" + c.DbPass + "@" + c.DbHost + "/" + c.DbName + "?sslmode=" + c.DbEncrypt
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		Logger.Debug(err)
	} else {
		c.db = db
	}
}

//关闭数据库
func (c *Conn) Close() {
	defer c.db.Close()
}

//执行数据库语句 <====这里根本没有数据返回========No Scan
func (c *Conn) Exec(sql string) []map[string]string {
	var rr = make([]map[string]string, 0)
	_, err := c.db.Exec(sql)
	if err != nil {
		Logger.Debug(err, "r")
	} else {
		Logger.Debug("pgsql Exec completed")
	}
	return rr
}

//数据查询
func (c *Conn) Proc(sql []string) [][]map[string]string {
	var rr = make([][]map[string]string, 0)
	//var cn string
	tx, err := c.db.Begin()
	if err != nil {
		Logger.Debug(err)
	}
	if len(sql) > 1 {
		_, err = tx.Exec(sql[0])
		if err != nil {
			Logger.Debug(err)
		}
		parm := sql[1:] //取查询FETCH ALL IN结果集
		for _, v := range parm {
			rows, err := tx.Query(v)
			defer rows.Close()
			if err != nil {
				Logger.Debug(err)
			} else {
				columns, _ := rows.Columns()
				scanArgs := make([]interface{}, len(columns))
				values := make([]interface{}, len(columns))
				for i := range values {
					scanArgs[i] = &values[i]
				}
				pr := make([]map[string]string, 0)
				for rows.Next() {
					err = rows.Scan(scanArgs...)
					if err != nil {
						Logger.Debug(err)
					}

					rp := make(map[string]string)
					for i, col := range values {
						if col != nil {
							coltype := reflect.TypeOf(col)
							var tocol string
							switch coltype.String() {
							case "int64":
								tocol = strconv.FormatInt(col.(int64), 10)
							case "float64":
								tocol = strconv.FormatFloat(col.(float64), 'G', 30, 64)
							case "string":
								tocol = col.(string)
							case "time.Time":
								tocol = col.(time.Time).String()
							case "bool":
								tocol = strconv.FormatBool(col.(bool))
							case "[]byte":
								tocol = string(col.([]byte))
							case "[]uint8":
								tocol = string(col.([]uint8))
							default:
								tocol = "notype"
							}
							rp[columns[i]] = tocol
						}
					}
					pr = append(pr, rp)
				}
				rr = append(rr, pr)
			}
		}
		if err := tx.Commit(); err != nil {
			Logger.Debug(err)
		}
	}
	return rr
}

//数据查询
func (c *Conn) Query(sql string) []map[string]string {
	var rr = make([]map[string]string, 0)
	rows, err := c.db.Query(sql)
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					coltype := reflect.TypeOf(col)
					var tocol string
					switch coltype.String() {
					case "int64":
						tocol = strconv.FormatInt(col.(int64), 10)
					case "float64":
						tocol = strconv.FormatFloat(col.(float64), 'G', 30, 64)
					case "string":
						tocol = col.(string)
					case "time.Time":
						tocol = col.(time.Time).String()
					case "bool":
						tocol = strconv.FormatBool(col.(bool))
					case "[]byte":
						tocol = string(col.([]byte))
					case "[]uint8":
						tocol = string(col.([]uint8))
					default:
						tocol = "notype"
					}
					rp[columns[i]] = tocol

					//rp[columns[i]] = string(col.([]byte))
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//获得数据
func (c *Conn) Take(take map[string]string) []map[string]string {
	//查询参数：1 字段=field、2 表名=table、3 WHERE=where、4 LIMIT=limit、5 ORDER=order
	var rr = make([]map[string]string, 0)
	var sql string
	var field string
	if k, ok := take["field"]; ok {
		field = k
	} else {
		field = "*"
	}
	/** table是必选项
	var table string
	if k, ok := take["table"]; ok {

	}
	**/
	var where string
	if k, ok := take["where"]; ok {
		where = " WHERE " + k
	}
	var limit string
	if k, ok := take["limit"]; ok {
		limit = " LIMIT " + k
	}
	var order string
	if k, ok := take["order"]; ok {
		order = " ORDER BY " + k
	}
	sql = "SELECT " + field + " FROM " + c.DbPrefix + take["table"] + where + order + limit
	rows, err := c.db.Query(sql)
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					coltype := reflect.TypeOf(col)
					var tocol string
					switch coltype.String() {
					case "int64":
						tocol = strconv.FormatInt(col.(int64), 10)
					case "float64":
						tocol = strconv.FormatFloat(col.(float64), 'G', 30, 64)
					case "string":
						tocol = col.(string)
					case "time.Time":
						tocol = col.(time.Time).String()
					case "bool":
						tocol = strconv.FormatBool(col.(bool))
					case "[]byte":
						tocol = string(col.([]byte))
					case "[]uint8":
						tocol = string(col.([]uint8))
					default:
						tocol = "notype"
					}
					rp[columns[i]] = tocol

					//rp[columns[i]] = string(col.([]byte))
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//保存数据
func (c *Conn) Save(keyvalue interface{}, save ...string) int64 {
	//保存参数：1 keyvalue=值map 、2 save[0]=表、3 save[1]=WHERE
	//如果有WHERE就UPDATE，否则INSERT INTO
	val := reflect.ValueOf(keyvalue)
	switch len(save) {
	case 1:
		var sql string
		var lastId int64
		var inkey = " ("
		var inval = " ("
		sql = "INSERT INTO " + c.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			inkey = inkey + tkey + ","
		}
		inkey = strings.TrimRight(inkey, ",")
		sql = sql + inkey + ") VALUES"
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tval := t.Index(1).String()
			inval = inval + tval + ","
		}
		inval = strings.TrimRight(inval, ",")
		sql = sql + inval + ") RETURNING id"
		err := c.db.QueryRow(sql).Scan(&lastId)
		if err != nil {
			Logger.Debug(err)
			return 0
		} else {
			//pgsql 不支持LastInsertId()函数
			return lastId
		}

	case 2:
		var sql string
		var sqlset = " SET "
		sql = "UPDATE " + c.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			tval := t.Index(1).String()
			sqlset = sqlset + tkey + "=" + tval + ","
		}
		sqlset = strings.TrimRight(sqlset, ",")
		sql = sql + sqlset + " WHERE " + save[1]
		_, err := c.db.Query(sql)
		if err != nil {
			Logger.Debug(err)
			return 0
		} else {
			return 1
		}
	default:
		return 0
	}

}

//删除数据
func (c *Conn) Delete(table, where string) bool {
	//删除参数：1 表名、2 WHERE
	checkdb := c.Query("SELECT '*' FROM " + c.DbPrefix + table + " WHERE " + where)
	if len(checkdb) > 0 {
		_, err := c.db.Query("DELETE FROM " + c.DbPrefix + table + " WHERE " + where)
		if err != nil {
			Logger.Debug(err)
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}
