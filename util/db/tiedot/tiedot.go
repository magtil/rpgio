package tiedot

import (
	"encoding/json"
	"strconv"

	"github.com/HouzuoGuo/tiedot/db"
	"github.com/HouzuoGuo/tiedot/dberr"
	"gitlab.com/magtil/rpgio/util/logger"
)

/**
*暂时只用 INT 和 STRING .......
**/
type Conn struct {
	Db     *db.DB
	DbFile string
}

var (
	Logger     = logger.Logger{}
	DbFolder   = "./db/"
	DateFormat = "2006/01/02/15:04:05"
)

//打开数据库
func (c *Conn) Open() {
	//这里可以设置一个data.Config的参数，设置数据库大小
	//应该再Conn的结构体里添加一个data.Config
	tdb, err := db.OpenDB(c.DbFile)
	if err != nil {
		Logger.Debug(err)
	} else {
		c.Db = tdb
	}
}

//关闭数据库
func (c *Conn) Close() {
	if err := c.Db.Close(); err != nil {
		Logger.Debug(err)
	}
}

//数据查询
func (c *Conn) Query(q string) []map[string]string {
	var rr = make([]map[string]string, 0)
	return rr
}

//数库执行
func (c *Conn) Exec(q string) interface{} {
	//这里就用json格式吧，创建就是{"Create":"one"}、{"Rename":["one","tow"]}、{"Drop":"two"}、{"Scrub":"one"}、、
	var qmap = make(map[string]interface{}, 0)
	err := json.Unmarshal([]byte(q), &qmap)
	if err != nil {
		return err
	} else {
		for k, v := range qmap {
			switch k {
			case "Create":
				Logger.Debug(v.(string))
				if err := c.Db.Create(v.(string)); err != nil {
					Logger.Debug(err)
					return err
				}
			case "Rename":
				if err := c.Db.Rename(v.([]interface{})[0].(string), v.([]interface{})[1].(string)); err != nil {
					Logger.Debug(err)
					return err
				}
			case "Drop":
				if err := c.Db.Drop(v.(string)); err != nil {
					Logger.Debug(err)
					return err
				}
			case "Scrub":
				if err := c.Db.Scrub(v.(string)); err != nil {
					Logger.Debug(err)
					return err
				}
			case "Index":
				feeds := c.Db.Use(v.([]interface{})[0].(string))
				if err := feeds.Index([]string{v.([]interface{})[1].(string)}); err != nil {
					Logger.Debug(err)
					return err
				}
			case "ColExists":
				return c.Db.ColExists(v.(string))
			case "AllCols":
				return c.Db.AllCols()
			}
		}
	}
	return nil
}

//获得数据
func (c *Conn) Take(t string, q map[string]string) []map[string]interface{} {

	var rr = make([]map[string]interface{}, 0)
	feeds := c.Db.Use(t)
	if len(q) > 0 {
		for k, v := range q {
			switch k {
			case "id":
				i, werr := strconv.Atoi(v)
				if werr != nil {
					Logger.Debug(werr)
				}
				readBack, err := feeds.Read(i)
				if err != nil {
					Logger.Debug(err)
				}
				Logger.Debug(readBack)
			case "query":
				//[{"eq":"Content here", "in":["Title"]},{"eq":"Content too","in":["Source"]}]
				var query interface{}
				json.Unmarshal([]byte(v), &query)
				queryResult := make(map[int]struct{})
				if err := db.EvalQuery(query, feeds, &queryResult); err != nil {
					Logger.Debug(err)
				}
				for id := range queryResult {
					readBack, err := feeds.Read(id)
					if err != nil {
						Logger.Debug(err)
					} else {
						readBack["id"] = id
						rr = append(rr, readBack)
						//最后就是这里，如何把结果放入rr里
					}
				}
			}
		}
	}
	return rr
}

//保存数据
func (c *Conn) Save(t string, kv map[string]interface{}, u ...string) int64 {
	//插入与更新
	feeds := c.Db.Use(t)
	docID := 0
	var err error = nil
	if len(u) > 0 {
		//更新
		i, werr := strconv.Atoi(u[0])
		if werr != nil {
			Logger.Debug(werr)
		}
		err = feeds.Update(i, kv)
		if err != nil {
			Logger.Debug(err)
		}
	} else {
		//插入
		docID, err = feeds.Insert(kv)
		if err != nil {
			Logger.Debug(err)
		}
	}

	return int64(docID)
}

//删除数据, return 还需要改为Save一样，设定一个变量，避免在错误是直接return.
func (c *Conn) Delete(t, w string) bool {
	//删除参数：1 表名、2 WHERE
	feeds := c.Db.Use(t)
	i, werr := strconv.Atoi(w)
	if werr != nil {
		Logger.Debug(werr)
	}
	if err := feeds.Delete(i); dberr.Type(err) != dberr.ErrorNoDoc {
		Logger.Debug(err)
		return false
	}
	return true
}
