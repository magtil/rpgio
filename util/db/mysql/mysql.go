package mysql

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"reflect"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/magtil/rpgio/util/logger"
)

var Logger = logger.Logger{}

type DBOper struct {
	db        *sql.DB
	DbHost    string
	DbPort    string
	DbName    string
	DbUser    string
	DbPass    string
	DbCharset string
	DbEncrypt string
	DbPrefix  string
}

//打开数据库
func (this *DBOper) DbOpen() {
	dsn := this.DbUser + ":" + this.DbPass + "@tcp(" + this.DbHost + ":" + this.DbPort + ")/" + this.DbName + "?charset=" + this.DbCharset
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		Logger.Debug(err)
	} else {
		this.db = db
	}
}

//关闭数据库
func (this *DBOper) DbClose() {
	defer this.db.Close()
}

//数据查询
func (f *DBOper) DbQuery(sql string) []map[string]string {
	var rr = make([]map[string]string, 0)
	rows, err := f.db.Query(sql)
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					rp[columns[i]] = string(col.([]byte))
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//获得数据
func (this *DBOper) Take(take map[string]string) []map[string]string {
	//查询参数：1 字段=field、2 表名=table、3 WHERE=where、4 LIMIT=limit、5 ORDER=order
	var rr = make([]map[string]string, 0)
	var sql string
	var field string
	if k, ok := take["field"]; ok {
		field = k
	} else {
		field = "*"
	}
	/** table是必选项
	var table string
	if k, ok := take["table"]; ok {

	}
	**/
	var where string
	if k, ok := take["where"]; ok {
		where = " WHERE " + k
	}
	var limit string
	if k, ok := take["limit"]; ok {
		limit = " LIMIT " + k
	}
	var order string
	if k, ok := take["order"]; ok {
		order = " ORDER BY " + k
	}
	sql = "SELECT " + field + " FROM " + this.DbPrefix + take["table"] + where + order + limit
	rows, err := this.db.Query(sql)
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					rp[columns[i]] = string(col.([]byte))
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//获得数据
func (this *DBOper) TakeOld(take ...string) []map[string]string {
	//查询参数：1 字段、2 表名、3 WHERE、4 LIMIT、5 ORDER
	var rr = make([]map[string]string, 0)
	var sql string
	switch len(take) {
	case 5:
		sql = "SELECT " + take[0] + " FROM " + this.DbPrefix + take[1] + " WHERE " + take[2] + " ORDER BY " + take[4] + " LIMIT " + take[3]
	case 4:
		sql = "SELECT " + take[0] + " FROM " + this.DbPrefix + take[1] + " WHERE " + take[2] + " LIMIT " + take[3]
	case 3:
		sql = "SELECT " + take[0] + " FROM " + this.DbPrefix + take[1] + " WHERE " + take[2]
	case 2:
		sql = "SELECT " + take[0] + " FROM " + this.DbPrefix + take[1]
	default:

	}
	//msgo.Info(sql, "31")
	rows, err := this.db.Query(sql)
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					rp[columns[i]] = string(col.([]byte))
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//保存数据
func (this *DBOper) Save(keyvalue interface{}, save ...string) int64 {
	//保存参数：1 keyvalue=值map 、2 save[0]=表、3 save[1]=WHERE
	//如果有WHERE就UPDATE，否则INSERT INTO
	val := reflect.ValueOf(keyvalue)
	switch len(save) {
	case 1:
		var sql string
		var inkey = " ("
		var inval = " ("
		sql = "INSERT INTO " + this.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			inkey = inkey + tkey + ","
		}
		inkey = strings.TrimRight(inkey, ",")
		sql = sql + inkey + ") VALUES"
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tval := t.Index(1).String()
			inval = inval + tval + ","
		}
		inval = strings.TrimRight(inval, ",")
		sql = sql + inval + ")"
		res, err := this.db.Exec(sql)
		if err != nil {
			Logger.Debug(err)
			return 0
		} else {
			reid, _ := res.LastInsertId()
			return reid
		}
	case 2:
		var sql string
		var sqlset = " SET "
		sql = "UPDATE " + this.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			tval := t.Index(1).String()
			sqlset = sqlset + tkey + "=" + tval + ","
		}
		sqlset = strings.TrimRight(sqlset, ",")
		sql = sql + sqlset + " WHERE " + save[1]
		_, err := this.db.Query(sql)
		if err != nil {
			Logger.Debug(err)
			return 0
		} else {
			return 1
		}
	default:
		return 0
	}

}

//删除数据
func (this *DBOper) Delete(table, where string) bool {
	//删除参数：1 表名、2 WHERE
	checkdb := this.DbQuery("SELECT '*' FROM " + this.DbPrefix + table + " WHERE " + where)
	if len(checkdb) > 0 {
		_, err := this.db.Query("DELETE FROM " + this.DbPrefix + table + " WHERE " + where)
		if err != nil {
			Logger.Debug(err)
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}

func ReadConfig(filename string) (map[string]string, error) {
	gets := map[string]string{}
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		Logger.Debug(err)
	}
	if err := json.Unmarshal(bytes, &gets); err != nil {
		Logger.Debug(err.Error())
		return nil, err
	}

	return gets, nil
}

/**
func checkErr(errMsg error) {
	if errMsg != nil {
		mo.Info(errMsg, "31")
	}
}
**/
