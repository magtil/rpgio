package user

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

func (u User) CreateToken(secret string, defDur int64, more ...string) string {
	//more[0] = issuer, more[1] = subject
	jwtIAT := time.Now().Unix() //签发时间
	iss := ""
	sub := ""
	if len(more) > 0 {
		iss = more[0]
	}

	switch len(more) {
	case 0:
		iss = "maggr"
		sub = ""
	case 1:
		iss = more[0]
	case 2:
		iss = more[0]
		sub = more[1]
	}

	claims := &jwt.StandardClaims{
		ExpiresAt: jwtIAT + defDur,
		IssuedAt:  jwtIAT,
		Issuer:    iss,
		Subject:   sub,
	}
	setClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := setClaims.SignedString([]byte(secret))
	if err != nil {
		Logger.Debug(err)
	}
	return token
}

func (u User) CheckToken(secret, tokenVal string) {
	Logger.Debug(secret)
	Logger.Debug(tokenVal)
	if len(tokenVal) > 10 {

		token, err := jwt.Parse(tokenVal, func(token *jwt.Token) (interface{}, error) {
			return []byte("dd"), nil
		})

		if token.Valid {
			Logger.Debug("You look nice today")

		} else if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				Logger.Debug("That's not even a token")
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				// Token is either expired or not active yet
				Logger.Debug("Timing is everything")
			} else {
				Logger.Debug("Couldn't handle this token:", err)
			}
		} else {
			Logger.Debug("Couldn't handle this token:", err)
		}

	}

}

func (u User) UpdateToken() {

}
