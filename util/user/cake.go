package user

import (
	"net/http"
	"time"
)

func (u User) SetCake(w http.ResponseWriter, cookieValue ...string) {
	//参数：1:直接w 2:cookieValue[0]=Cookie名 3:cookieValue[1]=Cookie值 4:cookieValue[2]=Cookie作用域 5:cookieValue[3]=Cookie有效时间
	defPath := "/"
	switch len(cookieValue) {
	case 2:
		cookie := http.Cookie{Name: cookieValue[0], Value: cookieValue[1], Path: defPath}
		http.SetCookie(w, &cookie)
	case 3:
		cookie := http.Cookie{Name: cookieValue[0], Value: cookieValue[1], Path: cookieValue[2]}
		http.SetCookie(w, &cookie)
	case 4:
		expiration := time.Now()
		switch cookieValue[3] {
		case "y":
			expiration = expiration.AddDate(1, 0, 0)
		case "m":
			expiration = expiration.AddDate(0, 1, 0)
		case "d":
			expiration = expiration.AddDate(0, 0, 1)
		case "h":
			expiration = expiration.Add(time.Hour)
		default:
			t, _ := time.ParseDuration(cookieValue[3])
			expiration = expiration.Add(t)
		}
		cookie := http.Cookie{Name: cookieValue[0], Value: cookieValue[1], Path: cookieValue[2], Expires: expiration}
		http.SetCookie(w, &cookie)
	}

}

//Get Cookie Value
func (u User) GetCake(r *http.Request, cakename string) *http.Cookie {
	recake := &http.Cookie{}
	recake, _ = r.Cookie(cakename)
	return recake
}

func (u User) DelCake() {}

//Get Cookie Value Output Error
func (u User) TakeCake(r *http.Request, cakename string) (*http.Cookie, error) {
	reCake, err := r.Cookie(cakename)
	return reCake, err
}
