package ui

//前端界面元素
type App struct {
	Router string `json:"router"`
	Title  string `json:"title"`
	Icon   int    `json:"icon"`
	Color  int    `json:"color"`
	Path   string `json:"path"`
}

//ui
type Panel struct {
	TopMenu []Elem `json:"topmenu"`
	//Operate  Operate `json:"operate"`
	StateBar []Elem `json:"statebar"`
	Focus    string `json:"focus"`
}

type Operate struct {
	Name   string `json:"name"`
	Lefts  []Elem `json:"lefts"`
	Rights []Elem `json:"rights"`
	Work   Work   `json:"work"`
}

//Lefts和Rights上的button应该有什么反应呢？载入Work区内容？URL？、打开Unit面板或其他设置面板？

type Work struct {
	Kind string      `json:"kind"`
	Head Header      `json:"head"`
	Body interface{} `json:"body"`
	Foot []Elem      `json:"foot"`
	//Modal interface{} `json:"modal"`
}

var LRKind = []string{"title", "text", "button", "tree"}

type Header struct {
	Nav     []Elem `json:"nav"`
	Title   string `json:"title"`
	Control []Elem `json:"control"`
}

type Form struct { //Elem中的Num值表示是否readOnly，1=true，0=false
	Title  string `json:"title"`
	Elem   []Elem `json:"elem"`
	Submit []Elem `json:"submit"`
}

type List struct {
	Title string   `json:"title"`
	Field []Elem   `json:"field"`
	Items [][]Elem `json:"items"`
}

type ListItem struct {
	Row []Elem `json:"row"`
}

type Doc struct {
	Title   string `json:"title"`
	Info    []Elem `json:"info"`
	Content []Elem `json:"content"`
}

//所有都是Elem，上面的都是使用Elem的情景
type Elem struct {
	Name   string      `json:"name"`   //名称,一般用作键
	Title  string      `json:"title"`  //标题，呈现的名称
	Action string      `json:"action"` //用作请求时的动作
	Value  interface{} `json:"value"`  //值
	Kind   string      `json:"kind"`   //Elem的种类
	Num    int64       `json:"num"`    //数值用，本应对应前端，一律用double，但应用在ID上最多, input中，1是只读
	Note   string      `json:"note"`   //备注
	Option string      `json:"option"` //选项，用作POST到哪里
	Sub    ElemSub     `json:"sub"`    //更多，用作于子内容
}

type ElemSub struct {
	Title string      `json:"title"`
	Kind  string      `json:"kind"`
	Item  interface{} `json:"item"`
}

//说明区,应该用map[string]string

//Elem的Kind并不是限定有那是类型，而是在什么的情况下的Elem有不同的Kind组。
//var ElemKind = []string{"action", "input", "textarea", "select", "switch", "checkbox", "text", "divider"}

var WorkKind = []string{"form", "list", "doc"}

//ElemKind应该分开Form所用的分类，因为Form应该有更多的不同类型
//Elem Kind:
var FormKind = []string{"input", "textarea", "select", "checkbox"}
var ModalKind = []string{"form", "list", "doc"}
var OptionKind = []string{"dialog", "modal", "set", "action"}

//已经没有了Key参数了
//var FormKey = []string{"new", "update"}
var FormSubmitKind = []string{"action", "post", "get"}

//表格列的宽度，intrinsic（自适应宽度），flex（占满剩余宽度），fixed可以用num设置指定宽度
var TableWidth = []string{"intrinsic", "flex", "fixed"}

//Elem Kind是否应该增加更多不同的操作类型呢？post、get是否直接归类到action里呢？dialog是否归类到modal？
