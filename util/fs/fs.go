package fs

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"

	"github.com/kardianos/osext"
	"gitlab.com/magtil/rpgio/util/logger"
)

type Fs struct{}

var (
	Logger = logger.Logger{}
)

func (f Fs) ReadJsonConfig(filename string) map[string]interface{} {
	var gets map[string]interface{}
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		Logger.Debug(err)
	}
	if err := json.Unmarshal(bytes, &gets); err != nil {
		Logger.Debug(err.Error())
		return nil
	}
	return gets
}

func (f Fs) StaticServer(prefix, staticDir string) {
	http.Handle(prefix, http.StripPrefix(prefix, http.FileServer(http.Dir(staticDir))))
}

//
func (f Fs) StaticFile(pattern, filename string) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	})
}

//重写一下静态下载服务
//prefix string, fileDir string, auth ...string
func (f Fs) DownloadServer(prefix, fileDir string, auth ...string) {
	http.HandleFunc(prefix, func(w http.ResponseWriter, r *http.Request) {
		if len(auth) > 0 {
			Logger.Debug("需要认证的(待开发):" + auth[0])
		} else {
			var tarDir = fileDir
			urlstr := r.URL.String()
			if len(urlstr) > len(prefix) {
				moreDir := urlstr[len(prefix):]
				tarDir = fileDir + "/" + moreDir
			}
			unDir, _ := url.QueryUnescape(tarDir)
			filepath, err := os.Stat(unDir)
			if err != nil {
				Logger.Debug(err)
			} else {
				if filepath.IsDir() {
					var filelist string
					for _, v := range f.ListDirFile(unDir) {
						filelist += v + "\n"
					}
					fmt.Fprintf(w, filelist)
				} else {
					http.ServeFile(w, r, unDir)
				}
			}
		}
	})
}

func (f Fs) ListDirFile(parm ...string) []string {
	var listfile = make([]string, 0)
	files, _ := ioutil.ReadDir(parm[0])
	if len(parm) > 1 {
		switch parm[1] {
		case "dir":
			for _, file := range files {
				if file.IsDir() {
					listfile = append(listfile, file.Name())
				} else {
				}
			}
		case "file":
			for _, file := range files {
				if file.IsDir() {
				} else {
					listfile = append(listfile, file.Name())
				}
			}
		default:
			for _, file := range files {
				if file.IsDir() {
					listfile = append(listfile, "["+file.Name()+"]")
				} else {
					listfile = append(listfile, file.Name())
				}
			}
		}
	} else {
		for _, file := range files {
			if file.IsDir() {
				listfile = append(listfile, "["+file.Name()+"]")
			} else {
				listfile = append(listfile, file.Name())
			}
		}
	}
	return listfile
}

// 判断文件还是文件夹
func (f Fs) CheckFile(filePath string) string {
	info, err := os.Stat(filePath)
	if err != nil {
		return "nill"
	} else if info.IsDir() {
		return "dir"
	} else {
		return "file"
	}
}

func (f Fs) CheckDirData(dirDataName string) string {
	if _, err := os.Stat(dirDataName); err == nil {
		dirData, err := filepath.Abs(dirDataName)
		if err != nil {
			panic(err)
		}
		return dirData
	}

	execPath, err := osext.Executable()
	if err != nil {
		panic(err)
	}

	execDir := filepath.Dir(execPath)
	path := filepath.Join(execDir, dirDataName)
	if _, err := os.Stat(path); err == nil {
		return path
	}

	return ""
}

//复制整个文件夹或单个文件
func (f Fs) Copy(from, to string) error {
	file, err := os.Stat(from)
	if err != nil {
		return err
	}
	if file.IsDir() {
		//from是文件夹，那么定义to也是文件夹
		if list, err := ioutil.ReadDir(from); err == nil {
			for _, item := range list {
				if err = f.Copy(filepath.Join(from, item.Name()), filepath.Join(to, item.Name())); err != nil {
					return err
				}
			}
		}
	} else {
		//from是文件，那么创建to的文件夹
		p := filepath.Dir(to)
		if _, err = os.Stat(p); err != nil {
			if err = os.MkdirAll(p, 0777); err != nil {
				return err
			}
		}
		//读取源文件
		file, err := os.Open(from)
		if err != nil {
			return err
		}
		defer file.Close()
		bufReader := bufio.NewReader(file)
		// 创建一个文件用于保存
		out, err := os.Create(to)
		if err != nil {
			return err
		}
		defer out.Close()
		// 然后将文件流和文件流对接起来
		_, err = io.Copy(out, bufReader)
	}
	return err
}
