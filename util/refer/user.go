package user

import (
	"crypto/md5"
	"encoding/hex"
	"myProject/Kappx/Base"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"
)

//var Oper = UserOper{}

type UserOper struct {
}

//dbkind应该在系统开始时设定，而不是现在的每次都查一次数据库！
//This is Cookie setting!
func (this UserOper) SetCake(w http.ResponseWriter, cookieValue ...string) {
	//参数：1:直接w 2:cookieValue[0]=Cookie名 3:cookieValue[1]=Cookie值 4:cookieValue[2]=Cookie作用域 5:cookieValue[3]=Cookie有效时间
	defPath := "/"
	switch len(cookieValue) {
	case 2:
		cookie := http.Cookie{Name: cookieValue[0], Value: cookieValue[1], Path: defPath}
		http.SetCookie(w, &cookie)
	case 3:
		cookie := http.Cookie{Name: cookieValue[0], Value: cookieValue[1], Path: cookieValue[2]}
		http.SetCookie(w, &cookie)
	case 4:
		expiration := time.Now()
		switch cookieValue[3] {
		case "y":
			expiration = expiration.AddDate(1, 0, 0)
		case "m":
			expiration = expiration.AddDate(0, 1, 0)
		case "d":
			expiration = expiration.AddDate(0, 0, 1)
		case "h":
			expiration = expiration.Add(time.Hour)
		default:
			t, _ := time.ParseDuration(cookieValue[3])
			expiration = expiration.Add(t)
		}
		cookie := http.Cookie{Name: cookieValue[0], Value: cookieValue[1], Path: cookieValue[2], Expires: expiration}
		http.SetCookie(w, &cookie)
	}

}

//Get Cookie Value
func (this UserOper) GetCake(r *http.Request, cakename string) *http.Cookie {
	recake := &http.Cookie{}
	recake, _ = r.Cookie(cakename)
	return recake
}

//Get Cookie Value Output Error
func (this UserOper) TakeCake(r *http.Request, cakename string) (*http.Cookie, error) {
	//recake := &http.Cookie{}
	recaketo, err := r.Cookie(cakename)
	return recaketo, err
}

func (this UserOper) GetCakes(r *http.Request) []*http.Cookie {
	return r.Cookies()
}

func (this UserOper) GetUserCookie(r *http.Request, cookiekey ...string) map[string]string {
	var remap = make(map[string]string)
	for _, k := range cookiekey {
		renote := &http.Cookie{}
		renote, _ = r.Cookie(k)
		if renote != nil {
			remap[renote.Name] = renote.Value
		}
	}
	return remap
}

func (this UserOper) CheckPassword(userkind string, ushell map[string]string, rawpass string) bool {
	oldUnix := this.StringTimeToUnix(ushell["logtime"], oper.TimeFormat)
	mdpass := md5.New()
	mdpass.Write([]byte(rawpass + oper.PassSub + oldUnix)) //登录验证的password
	topass := hex.EncodeToString(mdpass.Sum(nil))          //最终MD5值
	if topass == ushell["password"] {
		return true
	} else {
		return false
	}
}

func (this UserOper) SetPassword(userkind string, ushell map[string]string, newpass string) bool {
	oldUnix := this.StringTimeToUnix(ushell["logtime"], oper.TimeFormat)
	mdpass := md5.New()
	mdpass.Write([]byte(newpass + oper.PassSub + oldUnix)) //登录验证的password
	topass := hex.EncodeToString(mdpass.Sum(nil))          //最终MD5值

	var savePass int64
	if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
		savePass = oper.BaseDB.Save([][2]string{{"password", "'" + topass + "'"}}, "user_"+userkind, "name='"+ushell["name"]+"'")
	} else {
		takeArgs := []reflect.Value{reflect.ValueOf([][2]string{{"password", "'" + topass + "'"}}), reflect.ValueOf("user_" + userkind), reflect.ValueOf("name='" + ushell["name"] + "'")}
		takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Save").Call(takeArgs)[0]
		savePass = takeCall.Interface().(int64)
	}

	if savePass != 0 {
		return true
	} else {
		return false
	}

}

func (this UserOper) CheckPassport(userkind, user, passport string) []map[string]string {
	if len(user) > 1 {
		users := strings.Split(user, "|")
		username, _ := url.QueryUnescape(users[1])
		var userInfo []map[string]string
		var isql bool
		if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
			userInfo = oper.BaseDB.Take(map[string]string{
				"field": "id(),shell,name,password,logtime",
				"table": "user_" + userkind,
				"where": "id()=" + users[0] + " AND name='" + username + "'"})
			isql = true
		} else {
			var dbid string
			if oper.MemberDB[0]["kind"] == "ql" {
				dbid = "id()"
				isql = true
			} else {
				dbid = "id"
				isql = false
			}
			takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{
				"field": dbid + ",shell,name,password,logtime",
				"table": "user_" + userkind,
				"where": dbid + "=" + users[0] + " AND name='" + username + "'"})}
			takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]

			userInfo = takeCall.Interface().([]map[string]string)
		}
		if len(userInfo) > 0 {
			if isql {
				userInfo[0]["id"] = userInfo[0][""]
			}
			pused := string(userInfo[0]["id"] + userInfo[0]["name"] + userInfo[0]["password"] + userInfo[0]["logtime"])
			setPassport := md5.New()
			setPassport.Write([]byte(pused))
			passportValue := hex.EncodeToString(setPassport.Sum(nil))

			if passportValue == passport {
				return userInfo
			} else {
				return nil
			}

		} else {
			return nil
		}
	} else {
		return nil
	}
}

func (this UserOper) GetUserName(uid string) string {
	var reData string
	if oper.Settings["memberDB"] == "basedb" {
		userinfo := oper.BaseDB.Take(map[string]string{
			"table": "user_member",
			"where": "id()=" + uid + ""})
		reData = userinfo[0]["name"]
	} else {
		var dbid string
		if oper.MemberDB[0]["kind"] == "ql" {
			dbid = "id() as id"
		} else {
			dbid = "id"
		}
		getdata := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
			"table": "user_member",
			"where": dbid + "=" + uid + ""})
		re := getdata.([]map[string]string)
		reData = re[0]["name"]
	}
	return reData
}

func (this UserOper) GetUserInfo(userkind, username, password string) []map[string]string {
	if len(username) > 1 {
		var userShell []map[string]string
		if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
			userShell = oper.BaseDB.Take(map[string]string{
				"field": "id() as id,shell,name,password,logtime",
				"table": "user_" + userkind,
				"where": "name='" + username + "' AND password='" + password + "'"})
		} else {
			if oper.MemberDB[0]["kind"] == "ql" {
				/**
				takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{
					"field": "id() as id,shell,name,password,logtime",
					"table": "user_" + userkind,
					"where": "name='" + username + "' AND password='" + password + "'"})}
				takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]
				userShell = takeCall.Interface().([]map[string]string)
				**/
				userShell = oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
					"table": "user_" + userkind,
					"field": "id() as id,shell,name,password,email,createdate,logtime",
					"where": "name='" + username + "' AND password='" + password + "'"}).([]map[string]string)
			} else {
				/**
				takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{
					"table": "user_" + userkind,
					"where": "name='" + username + "' AND password='" + password + "'"})}
				takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]
				userShell = takeCall.Interface().([]map[string]string)
				**/
				userShell = oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
					"table": "user_" + userkind,
					"where": "name='" + username + "' AND password='" + password + "'"}).([]map[string]string)
			}
		}
		if len(userShell) > 0 {
			return userShell
		} else {
			return nil
		}
	} else {
		return nil
	}
}

func (this UserOper) GetIDToUserInfo(userkind, userid string) []map[string]string {
	if len(userid) > 0 {
		var userShell []map[string]string
		if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
			userShell = oper.BaseDB.Take(map[string]string{
				"table": "user_" + userkind,
				"field": "id() as id,shell,name,password,email,createdate,logtime",
				"where": "id()=" + userid + ""})
		} else {
			if oper.MemberDB[0]["kind"] == "ql" {
				/**
				takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{
					"table": "user_" + userkind,
					"field": "id() as id,shell,name,password,email,createdate,logtime",
					"where": "id()=" + userid + ""})}
				takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]
				userShell = takeCall.Interface().([]map[string]string)
				**/
				userShell = oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
					"table": "user_" + userkind,
					"field": "id() as id,shell,name,password,email,createdate,logtime",
					"where": "id()=" + userid + ""}).([]map[string]string)
			} else {
				/**
				takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{
					"table": "user_" + userkind,
					"where": "id=" + userid + ""})}
				takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]
				userShell = takeCall.Interface().([]map[string]string)
				**/
				userShell = oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
					"table": "user_" + userkind,
					"where": "id=" + userid + ""}).([]map[string]string)
			}
		}
		if len(userShell) > 0 {
			return userShell
		} else {
			return nil
		}
	} else {
		return nil
	}
}

func (this UserOper) GetAvatar(unitname string) []map[string]string {
	var reData []map[string]string
	if oper.Settings["memberDB"] == "basedb" {
		if unitname == "all" {
			reData = oper.BaseDB.Take(map[string]string{
				"field": "id() as id,uid,mold,unit,sort,title",
				"table": "user_avatar"})
		} else {
			reData = oper.BaseDB.Take(map[string]string{
				"field": "id() as id,uid,mold,unit,sort,title",
				"table": "user_avatar",
				"where": "unit='" + unitname + "'"})
		}
	} else {
		var dbid string
		if oper.MemberDB[0]["kind"] == "ql" {
			dbid = "id() as id"
		} else {
			dbid = "id"
		}
		if unitname == "all" {
			getdata := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
				"field": dbid + ",uid,mold,unit,sort,title",
				"table": "user_avatar"})
			reData = getdata.([]map[string]string)
		} else {
			getdata := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
				"field": dbid + ",uid,mold,unit,sort,title",
				"table": "user_avatar",
				"where": "unit='" + unitname + "'"})
			reData = getdata.([]map[string]string)
		}
	}
	return reData
}

func (this UserOper) AddAvatar(moldname, unitname, sort, userid string) (string, string) {
	var reInt int64
	var reInfo string
	var checkAvatar []map[string]string
	if oper.Settings["memberDB"] == "basedb" {
		checkAvatar = oper.BaseDB.Take(map[string]string{
			"table": "user_avatar",
			"where": "unit='" + unitname + "' AND sort=" + sort + " AND uid=" + userid})
	} else {
		checkDB := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
			"table": "user_avatar",
			"where": "unit='" + unitname + "' AND sort=" + sort + " AND uid=" + userid})
		checkAvatar = checkDB.([]map[string]string)
	}
	if len(checkAvatar) > 0 {
		reInt, reInfo = 0, "该身份已经设置过了。"
	} else {
		nowTime := time.Now().Unix()
		stringTime := strconv.FormatInt(nowTime, 10)
		if oper.Settings["memberDB"] == "basedb" {
			sortinfo := oper.BaseDB.Take(map[string]string{
				"table": "user_avatar_sort",
				"where": "id() = " + sort})

			saveArray := [...][2]string{{"uid", "" + userid + ""}, {"mold", "'" + moldname + "'"}, {"unit", "'" + unitname + "'"}, {"sort", "" + sort + ""}, {"title", "'" + sortinfo[0]["title"] + "'"}, {"level", "'" + sortinfo[0]["level"] + "'"}, {"createdate", "'" + stringTime + "'"}}
			save := oper.BaseDB.Save(saveArray, "user_avatar")
			if save > 0 {
				reInt, reInfo = save, "ok"
			} else {
				reInt, reInfo = save, "Save Fail"
			}
		} else {
			var dbid string
			if oper.MemberDB[0]["kind"] == "ql" {
				dbid = "id()"
			} else {
				dbid = "id"
			}
			sortDB := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
				"table": "user_avatar_sort",
				"where": dbid + " = " + sort})
			sortinfo := sortDB.([]map[string]string)
			saveArray := [...][2]string{{"uid", "" + userid + ""}, {"mold", "'" + moldname + "'"}, {"unit", "'" + unitname + "'"}, {"sort", "" + sort + ""}, {"title", "'" + sortinfo[0]["title"] + "'"}, {"level", "'" + sortinfo[0]["level"] + "'"}, {"createdate", "'" + stringTime + "'"}}
			saveTo := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Save", saveArray, "user_avatar")
			save := saveTo.(int64)
			if save > 0 {
				reInt, reInfo = save, "ok"
			} else {
				reInt, reInfo = save, "Save Fail"
			}
		}
	}
	return strconv.FormatInt(reInt, 10), reInfo
}

func (this UserOper) DeleteAvatar(delid string) (string, string) {
	var reInt string
	var reInfo string
	if oper.Settings["memberDB"] == "basedb" {
		dbre := oper.BaseDB.Delete("user_avatar", "id()="+delid)
		if dbre == true {
			reInt, reInfo = "0", "删除成功"
		} else {
			reInt, reInfo = "1", "删除时出错"
		}
	} else {
		var dbid string
		if oper.MemberDB[0]["kind"] == "ql" {
			dbid = "id()"
		} else {
			dbid = "id"
		}
		dbre := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Delete", "user_avatar", dbid+"="+delid).(bool)
		if dbre == true {
			reInt, reInfo = "0", "删除成功"
		} else {
			reInt, reInfo = "1", "删除时出错"
		}
	}
	return reInt, reInfo
}

//在这里，添加一个不定长变参，如果有参数，就。。。？看看代码先
func (this UserOper) GetAvatarSort(unitname string, whereid ...string) []map[string]string {
	var reData []map[string]string
	if len(whereid) > 0 {
		if oper.Settings["memberDB"] == "basedb" {
			reData = oper.BaseDB.Take(map[string]string{
				"field": "id() as id,unit,parentid,title,listorder,level",
				"table": "user_avatar_sort",
				"order": "listorder",
				"where": "unit='" + unitname + "' AND id()=" + whereid[0]})
		} else {
			var dbid string
			var wid string
			if oper.MemberDB[0]["kind"] == "ql" {
				dbid = "id() as id"
				wid = "id()"
			} else {
				dbid = "id"
				wid = "id"
			}
			getdata := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
				"field": dbid + ",unit,parentid,title,listorder,level",
				"table": "user_avatar_sort",
				"order": "listorder",
				"where": "unit='" + unitname + "' AND " + wid + "=" + whereid[0]})
			reData = getdata.([]map[string]string)
		}
	} else {
		if oper.Settings["memberDB"] == "basedb" {
			reData = oper.BaseDB.Take(map[string]string{
				"field": "id() as id,unit,parentid,title,listorder,level",
				"table": "user_avatar_sort",
				"order": "listorder",
				"where": "unit='" + unitname + "'"})
		} else {
			var dbid string
			if oper.MemberDB[0]["kind"] == "ql" {
				dbid = "id() as id"
			} else {
				dbid = "id"
			}
			getdata := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
				"field": dbid + ",unit,parentid,title,listorder,level",
				"table": "user_avatar_sort",
				"order": "listorder",
				"where": "unit='" + unitname + "'"})
			reData = getdata.([]map[string]string)
		}
	}

	return reData
}

func (this UserOper) AddAvatarSort(moldname, unitname, title string) (string, string) {
	var reInt string
	var reInfo string
	var checkTitle []map[string]string

	if oper.Settings["memberDB"] == "basedb" {
		checkTitle = oper.BaseDB.Take(map[string]string{
			"table": "user_avatar_sort",
			"where": "unit='" + unitname + "' AND title='" + title + "'"})
	} else {
		checkDB := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
			"table": "user_avatar_sort",
			"where": "unit='" + unitname + "' AND title='" + title + "'"})
		checkTitle = checkDB.([]map[string]string)
	}
	if len(checkTitle) > 0 {
		reInt, reInfo = "1", "该分类名称已经设置过。"
	} else {
		saveArray := [...][2]string{{"mold", "'" + moldname + "'"}, {"unit", "'" + unitname + "'"}, {"parentid", "0"}, {"title", "'" + title + "'"}}
		if oper.Settings["memberDB"] == "basedb" {
			save := oper.BaseDB.Save(saveArray, "user_avatar_sort")
			if save > 0 {
				reInt = "0"
				reInfo = "Save OK!"
			} else {
				reInt = "1"
				reInfo = "Save Fail!"
			}
		} else {
			saveTo := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Save", saveArray, "user_avatar_sort")
			save := saveTo.(int64)
			if save > 0 {
				reInt = "0"
				reInfo = "Save OK!"
			} else {
				reInt = "1"
				reInfo = "Save Fail!"
			}
		}
	}
	return reInt, reInfo
}

func (this UserOper) UpdateAvatarSort(saveArray [][2]string, whereid string) bool {
	var reInfo bool
	if oper.Settings["memberDB"] == "basedb" {
		save := oper.BaseDB.Save(saveArray, "user_avatar_sort", "id()="+whereid+"")
		if save > 0 {
			reInfo = true
		} else {
			reInfo = false
		}
	} else {
		var dbid string
		if oper.MemberDB[0]["kind"] == "ql" {
			dbid = "id()"
		} else {
			dbid = "id"
		}
		saveTo := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Save", saveArray, "user_avatar_sort", dbid+"="+whereid+"")
		save := saveTo.(int64)
		if save > 0 {
			reInfo = true
		} else {
			reInfo = false
		}
	}
	return reInfo
}

func (this UserOper) DeleteAvatarSort(moldname, unitname string) {
	oper.MSG.Info(moldname)
	oper.MSG.Info(unitname)
}

func (this UserOper) CheckTest() bool {
	return true
}

func (this UserOper) CheckSignVerify(r *http.Request, userkind, username, passport, level string) bool {
	var isLogin bool
	uShell := this.CheckPassport(userkind, username, passport)
	if len(uShell) > 0 {
		intshell, _ := strconv.ParseInt(uShell[0]["shell"], 10, 0)
		intlevel, _ := strconv.ParseInt(level, 10, 0)

		if intshell >= intlevel {
			isLogin = true
		} else {
			isLogin = false
		}
	} else {
		isLogin = false
	}
	return isLogin
}

func (this UserOper) UserSignIn(w http.ResponseWriter, val ...string) map[string]string {
	//使用不定长变参吧，后面可以设置Cookie时长,增加用户身份
	//val[0]=id(错了吧??????????应该是User Kind?),val[1]=username,val[2]=password,val[3]=level,val[4]=cookie time
	var reinfo = make(map[string]string)
	//没有检测用户是否存在就去取用户LogTime，鬼都出错啦！！！

	var getUserLogtime []map[string]string
	if val[0] == "root" || oper.Settings["memberDB"] == "basedb" {
		getUserLogtime = oper.BaseDB.Take(map[string]string{
			"field": "logtime",
			"table": "user_" + val[0],
			"where": "name='" + val[1] + "'"})
	} else {
		/**
		takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{
			"field": "logtime",
			"table": "user_" + val[0],
			"where": "name='" + val[1] + "'"})}
		takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]
		getUserLogtime = takeCall.Interface().([]map[string]string)
		**/
		queryMap := map[string]string{
			"field": "logtime",
			"table": "user_" + val[0],
			"where": "name='" + val[1] + "'"}
		getUserLogtime = oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", queryMap).([]map[string]string)
	}
	if len(getUserLogtime) > 0 {
		oldUnix := this.StringTimeToUnix(getUserLogtime[0]["logtime"], oper.TimeFormat)
		mdpass := md5.New()
		mdpass.Write([]byte(val[2] + oper.PassSub + oldUnix)) //登录验证的password
		topass := hex.EncodeToString(mdpass.Sum(nil))         //最终MD5值
		ushell := this.GetUserInfo(val[0], val[1], topass)
		if len(ushell) > 0 {
			intshell, _ := strconv.ParseInt(ushell[0]["shell"], 10, 0)
			intlevel, _ := strconv.ParseInt(val[3], 10, 0)

			if intshell >= intlevel {
				dbLogTime := time.Now().Format(oper.TimeFormat)
				stringUnix := this.StringTimeToUnix(dbLogTime, oper.TimeFormat)
				setPassword := md5.New()
				setPassword.Write([]byte(val[2] + oper.PassSub + stringUnix))
				newPassword := hex.EncodeToString(setPassword.Sum(nil))

				saveArray := [...][2]string{{"password", "'" + newPassword + "'"}, {"logtime", "'" + dbLogTime + "'"}}

				if val[0] == "root" || oper.Settings["memberDB"] == "basedb" {
					oper.BaseDB.Save(saveArray, "user_"+val[0], "id()="+ushell[0]["id"])
				} else {
					var dbid string
					if oper.MemberDB[0]["kind"] == "ql" {
						dbid = "id()"
					} else {
						dbid = "id"
					}
					takeArgs := []reflect.Value{reflect.ValueOf(saveArray), reflect.ValueOf("user_" + val[0]), reflect.ValueOf(dbid + "='" + ushell[0]["id"] + "'")}
					reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Save").Call(takeArgs)
					//savePass = takeCall.Interface().(int64)
				}

				pused := string(ushell[0]["id"] + ushell[0]["name"] + newPassword + dbLogTime)
				setPassport := md5.New()
				setPassport.Write([]byte(pused))
				passportValue := hex.EncodeToString(setPassport.Sum(nil))
				var cookieName [2]string
				if val[0] == "root" {
					cookieName[0] = "admin"
					cookieName[1] = "adminpassport"
				} else {
					cookieName[0] = "member"
					cookieName[1] = "passport"
				}
				switch len(val) {
				case 4:
					this.SetCake(w, cookieName[0], ushell[0]["id"]+"|"+url.QueryEscape(ushell[0]["name"]))
					//this.SetCake(w, cookieName[0], ushell[0]["id"]+"|"+ushell[0]["name"])
					this.SetCake(w, cookieName[1], passportValue)
				case 5:
					this.SetCake(w, cookieName[0], ushell[0]["id"]+"|"+url.QueryEscape(ushell[0]["name"]), "/", val[4])
					//this.SetCake(w, cookieName[0], ushell[0]["id"]+"|"+ushell[0]["name"], "/", val[4])
					this.SetCake(w, cookieName[1], passportValue, "/", val[4])
				}

				oper.MSG.Info("["+ushell[0]["name"]+"]登录了", "p")

				reinfo["reError"] = "0"
				reinfo["reMsg"] = "验证通过！"
			} else {
				reinfo["reError"] = "1"
				reinfo["reMsg"] = "权限不够！"
			}
		} else {
			oper.MSG.Info("["+val[1]+"]登录时密码验证出错！", "r")
			reinfo["reError"] = "1"
			reinfo["reMsg"] = "密码错误！是否设置了大写锁定？"
		}
	} else {
		oper.MSG.Info("用户名["+val[1]+"]尝试登录，但该用户不存在！", "r")
		reinfo["reError"] = "1"
		reinfo["reMsg"] = "找不到该用户！！"
	}
	//返回信息
	return reinfo
}

func (this UserOper) UserSignOut(w http.ResponseWriter, user, passport string) map[string]string {
	var reinfo = make(map[string]string)
	this.SetCake(w, user, "", "/", "0")
	this.SetCake(w, passport, "", "/", "0")

	reinfo["reError"] = "0"
	reinfo["reMsg"] = "你已退出登录"
	return reinfo
}

func (this UserOper) DeleteCake(w http.ResponseWriter, cakename string) map[string]string {
	var reinfo = make(map[string]string)
	this.SetCake(w, cakename, "", "/", "0")

	reinfo["reError"] = "0"
	reinfo["reMsg"] = "Cookie已删除。"
	return reinfo
}

func (this UserOper) CreateUser(userkind string, savedata map[string]string) string {
	dbLogTime := time.Now().Format(oper.TimeFormat)

	var checkUser []map[string]string
	if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
		checkUser = oper.BaseDB.Take(map[string]string{"table": "user_" + userkind, "where": "name='" + savedata["name"] + "'"})
	} else {

		dbuserlist := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{"table": "user_" + userkind, "where": "name='" + savedata["name"] + "'"})
		checkUser = dbuserlist.([]map[string]string)
	}
	if len(checkUser) == 0 {
		stringUnix := this.StringTimeToUnix(dbLogTime, oper.TimeFormat)
		setPassword := md5.New()
		setPassword.Write([]byte(savedata["password"] + oper.PassSub + stringUnix))
		newPassword := hex.EncodeToString(setPassword.Sum(nil))
		var saveArray = [...][2]string{{"shell", "" + savedata["shell"] + ""}, {"name", "'" + savedata["name"] + "'"}, {"password", "'" + newPassword + "'"}, {"email", "'" + savedata["email"] + "'"}, {"createdate", "'" + dbLogTime + "'"}, {"logtime", "'" + dbLogTime + "'"}}

		var save int64
		if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
			save = oper.BaseDB.Save(saveArray, "user_"+userkind)
		} else {

			saveTo := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Save", saveArray, "user_"+userkind)
			save = saveTo.(int64)

		}

		if save > 0 {
			return "OK"
		} else {
			return "Create User Error!"
		}
	} else {
		return "用户已存在！"
	}
}

func (this UserOper) GetUserList(userkind string) []map[string]string {
	var dbuserlist []map[string]string
	/**
		if oper.Settings["memberDB"] == "basedb" {
			dbuserlist = oper.BaseDB.Take(map[string]string{"table": "user_member"})
		} else {
			takeArgs := []reflect.Value{reflect.ValueOf(map[string]string{"table": "user_member"})}
			takeCall := reflect.ValueOf(oper.DB[oper.Settings["memberDB"]]).MethodByName("Take").Call(takeArgs)[0]
			dbuserlist = takeCall.Interface().([]map[string]string)
		}
	**/
	//oper.MSG.Info(oper.MemberDB)
	if userkind == "root" || oper.Settings["memberDB"] == "basedb" {
		dbuserlist = oper.BaseDB.Take(map[string]string{
			"field": "id() as id,shell,name,password,email,createdate,logtime",
			"table": "user_" + userkind})
	} else {
		var dbid string
		if oper.MemberDB[0]["kind"] == "ql" {
			dbid = "id() as id"
		} else {
			dbid = "id"
		}

		userList := oper.DbOper(oper.DB[oper.Settings["memberDB"]], "Take", map[string]string{
			"field": dbid + ",shell,name,password,email,createdate,logtime",
			"table": "user_" + userkind})

		dbuserlist = userList.([]map[string]string)
	}

	return dbuserlist
}

func (this UserOper) StringTimeToUnix(tstring, tformat string) string {
	totimetype, _ := time.Parse(tformat, tstring)
	tounix := strconv.FormatInt(totimetype.Unix(), 10)
	return tounix
}

/**
func (this UserOper) UserSignUp(singupinfo map[string]string) {

}
**/
