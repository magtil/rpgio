package kit

type InitConfig struct {
	DbFolder   string
	PassObfus  string
	DateFormat string
	Version    string
}

type Cfgs struct { //TOML Config
	Base   map[string]interface{}
	Folder map[string]string
	Sys    map[string]string
}

type BaseDBInit struct {
	Collections []map[string]interface{}
	Data        []map[string]interface{}
}

//json通讯格式
type Action struct {
	Opt string      `json:"opt"`
	Act string      `json:"act"`
	Msg string      `json:"msg"`
	Dat interface{} `json:"dat"`
}

type Result struct {
	Err int64       `json:"err"`
	Msg string      `json:"msg"`
	Dat interface{} `json:"dat"`
}
