package kit

import (
	"encoding/json"
	"io/ioutil"

	"github.com/BurntSushi/toml"
	"gitlab.com/magtil/rpgio/util/db"
	"gitlab.com/magtil/rpgio/util/db/boltdb"
	"gitlab.com/magtil/rpgio/util/db/mssql"
	"gitlab.com/magtil/rpgio/util/db/pgsql"
	"gitlab.com/magtil/rpgio/util/db/ql"
	"gitlab.com/magtil/rpgio/util/fs"
	"gitlab.com/magtil/rpgio/util/logger"
	"gitlab.com/magtil/rpgio/util/str"
	"gitlab.com/magtil/rpgio/util/user"
)

var (
	Cfg Cfgs
	Ini = InitConfig{
		DbFolder:   "./db/",
		PassObfus:  "bullcalf",
		DateFormat: "060102,150405",
		Version:    "0.1.0"}
	Molds  = make([]string, 0) //这里应该加入更多信息，把Unit的初始化执行列表也放进去？？
	Units  = make(map[string]interface{})
	BaseDB = "base.ql"
	EvCh   = make(chan string)
)

//taleser operations get
var (
	Logger = logger.Logger{}
	Db     = db.Db{}
	Fs     = fs.Fs{}
	Str    = str.Str{}
	User   = user.User{}
)

func DbInit(dbmap map[string]interface{}) {
	switch dbmap["kind"].(string) {
	case "boltdb":
		Db.List[dbmap["name"].(string)] = &boltdb.Conn{DbFile: Ini.DbFolder + dbmap["db_host"].(string)}
	case "ql":
		Db.List[dbmap["name"].(string)] = &ql.Conn{DbFile: Ini.DbFolder + dbmap["db_host"].(string)}
	case "pgsql":
		Db.List[dbmap["name"].(string)] = &pgsql.Conn{DbHost: dbmap["db_host"].(string),
			DbPort:    dbmap["db_port"].(string),
			DbName:    dbmap["db_name"].(string),
			DbUser:    dbmap["db_user"].(string),
			DbPass:    dbmap["db_pass"].(string),
			DbCharset: dbmap["db_charset"].(string),
			DbEncrypt: dbmap["db_encrypt"].(string),
			DbPrefix:  dbmap["db_prefix"].(string)}
	case "mssql":
		Db.List[dbmap["name"].(string)] = &mssql.Conn{DbHost: dbmap["db_host"].(string),
			DbPort:    dbmap["db_port"].(string),
			DbName:    dbmap["db_name"].(string),
			DbUser:    dbmap["db_user"].(string),
			DbPass:    dbmap["db_pass"].(string),
			DbCharset: dbmap["db_charset"].(string),
			DbEncrypt: dbmap["db_encrypt"].(string),
			DbPrefix:  dbmap["db_prefix"].(string)}

	}
}

type InitData struct {
	InitList    []string `json:"init"`
	InitVersion string   `json:"version"`
}

func init() {
	//为特定工具自定义参数
	cfgFile, err := ioutil.ReadFile("./cfg/base.toml")
	if err != nil {
		Logger.Error(err)
	}
	_, err = toml.Decode(string(cfgFile), &Cfg)
	if err != nil {
		Logger.Error(err)
	}

	//初始化变量
	if len(Cfg.Base["DateFormat"].(string)) > 0 {
		Ini.DateFormat = Cfg.Base["DateFormat"].(string)
	}
	if len(Cfg.Folder["Db"]) > 0 {
		Ini.DbFolder = Cfg.Folder["Db"]
	}

	//所有的初始化都放到这里吧，别再纠结了

	//需要初始化的to

	Logger = logger.Init(logger.Logger{
		DbFolder:   Ini.DbFolder,
		DateFormat: Ini.DateFormat})

	//初始化基础库
	Db = db.Init(db.Db{
		DbFolder:   Ini.DbFolder,
		DateFormat: Ini.DateFormat})

	if Fs.CheckFile(Ini.DbFolder+BaseDB) == "file" {
		Db.List["base"] = &ql.Conn{DbFolder: Ini.DbFolder, DbFile: Ini.DbFolder + BaseDB}
		Db.Open("base")
	} else {
		Logger.Debug("initialize base DB")
		Db.List["base"] = &ql.Conn{DbFile: Ini.DbFolder + BaseDB}
		Db.Open("base")
		var initData InitData
		initConfig, err := ioutil.ReadFile("./cfg/initialize/init.json")
		if err != nil {
			Logger.Error(err)
		}
		if err := json.Unmarshal(initConfig, &initData); err != nil {
			Logger.Error(err)
		}
		for _, v := range initData.InitList {
			Db.Exec("base", v)
			Logger.Debug("Exec:[" + v + "]")
		}
	}
	//创建用户会话库
	Db.List["session"] = &boltdb.Conn{DbFile: Ini.DbFolder + "session.db"}
	Db.Open("session")

	//读取基础库配置
	dbSettings := Db.Take("base", "settings", map[string]string{})
	var formatSetting = make(map[string]string)
	for _, v := range dbSettings {
		formatSetting[v["setkey"].(string)] = v["setval"].(string)
	}
	Cfg.Sys = formatSetting
	dbList := Db.Take("base", "db", map[string]string{})
	if len(dbList) > 0 {
		Logger.Log("init dbs...")
		for _, v := range dbList {
			DbInit(v)
		}
	}

	User = user.Init(user.User{
		PassObfus:  Ini.PassObfus,
		DateFormat: Ini.DateFormat,
		Db:         Db})

}
